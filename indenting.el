;; Emacs indent settings
(mapcar #'(lambda (x) (put x 'scheme-indent-function 1))
	'(transition extend-transition
	  subscribe subscribe/fresh unsubscribe
	  os-big-bang
	  message-handlers meta-message-handlers ground-message-handler))
