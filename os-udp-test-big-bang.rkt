#lang racket/base
;; Trivial example program demonstrating os-udp.rkt working with os-big-bang.

(require racket/match)
(require "os-big-bang.rkt")
(require (only-in "os-userland.rkt" userland))
(require "os-udp.rkt")
(require "os-big-bang-testing.rkt")

(define (packet-handler sname)
  (message-handlers w
   [(udp-packet source (== sname) body)
    (transition w
		(send-message (udp-packet sname source body)))]))

(check-message-handler (packet-handler (udp-address #f 5555))
		       'none
		       (udp-packet (udp-address "127.0.0.1" 12345) (udp-address #f 5555) #"abcd")
		       'none
		       (list (send-message (udp-packet (udp-address #f 5555)
						       (udp-address "127.0.0.1" 12345)
						       #"abcd"))))

(define echoer
  (os-big-bang 'none
    (send-message `(request create-echo-socket (udp new 5555 65536)))
    (subscribe/fresh sub
      (message-handlers w
	[`(reply create-echo-socket ,sname)
	 (transition w
	   (unsubscribe sub)
	   (subscribe 'packet-handler (packet-handler sname)))]))))

(define spy
  (os-big-bang 'none
    (subscribe 'spy (message-handlers w [x (write `(MESSAGE ,x)) (newline)]))))

(define (main)
  (ground-vm
   (os-big-bang 'none
		(spawn spy)
		(spawn udp-driver)
		(spawn echoer))))

(main)
;;(provide main)
