;; Emacs indent settings
(progn
  (mapcar #'(lambda (x) (put x 'scheme-indent-function 1))
	  '(role transition sequence-actions))
  (mapcar #'(lambda (x) (put x 'scheme-indent-function 2))
	  '(role/fresh yield)))
