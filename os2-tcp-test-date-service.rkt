#lang racket/base
;; Differently trivial example program demonstrating os2-tcp.rkt.

(require racket/string)
(require racket/set)
(require racket/match)
(require "os2.rkt")
(require "os2-tcp.rkt")

(define ((connection-handler local-addr remote-addr) self-pid)
  (transition 'no-state
    (role (topic-publisher (tcp-channel local-addr remote-addr (wild)))
      [(tcp-channel _ _ (tcp-credit _)) (quit)])
    (send-message (tcp-channel local-addr remote-addr
			       (string->bytes/utf-8
				(format "~a\n" (current-inexact-milliseconds)))))))

(define (listener local-addr)
  (transition 'no-state
    (role (topic-subscriber (tcp-channel (wild) local-addr (wild)) #:monitor? #t)
      #:topic t
      #:on-presence (match t
		      [(topic 'publisher (tcp-channel remote-addr (== local-addr) _) #f)
		       (spawn (connection-handler local-addr remote-addr))]))))

(define (main port)
  (ground-vm
   (transition 'none
     ;; (spawn tcp-spy)
     (spawn tcp-driver)
     (spawn (listener (tcp-listener port))))))

(main 5999)
